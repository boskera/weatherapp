﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WeatherApp {
    public partial class Form1 : Form {
        Vrijeme vrijeme;
        public Form1() {
            InitializeComponent();
            vrijeme = new Vrijeme();
        }

        private void Form1_Load(object sender, EventArgs e) {
            foreach(var grad in vrijeme.DohvatiGradove()) {
                cbGradovi.Items.Add(grad);
            }
        }

        private void OsvjeziPrikaz() {
            lbNajtopliji.Items.Clear();
            lbNajhladniji.Items.Clear();
        }

        private void cbGradovi_TextChanged(object sender, EventArgs e) {
        }
    }
}
