﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WeatherApp {
    class Vrijeme {
        public Vrijeme() {
            vrijeme = new XmlDocument();
            Osvjezi();
        }

        public void Osvjezi() {
            vrijeme.Load("http://vrijeme.hr/hrvatska_n.xml");
            DohvatiPodatke();
        }

        public List<VrijemePodatak> DohvatiPodatke() {
            List<VrijemePodatak> podaci = new List<VrijemePodatak>();

            XmlNodeList gradovi = vrijeme.GetElementsByTagName("Grad");
            foreach (XmlNode grad in gradovi) {
                podaci.Add(new VrijemePodatak(
                    grad["GradIme"].InnerText,
                    grad["Podatci"]["Temp"].InnerText,
                    grad["Podatci"]["Vlaga"].InnerText,
                    grad["Podatci"]["Tlak"].InnerText
                ));
            }
            return podaci;
        }

        public IEnumerable<string> DohvatiGradove() {
            return
                from podatak in podaci
                orderby podatak.Grad
                select podatak.Grad;
        }

        private List<VrijemePodatak> podaci;
        private XmlDocument vrijeme;
    }
}
