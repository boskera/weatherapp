﻿
namespace WeatherApp {
    partial class Form1 {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.cbGradovi = new System.Windows.Forms.ComboBox();
            this.lblTemperatura = new System.Windows.Forms.Label();
            this.lblVlaga = new System.Windows.Forms.Label();
            this.lblTlak = new System.Windows.Forms.Label();
            this.lblNajtopliji = new System.Windows.Forms.Label();
            this.lblNajhladniji = new System.Windows.Forms.Label();
            this.lbNajtopliji = new System.Windows.Forms.ListBox();
            this.lbNajhladniji = new System.Windows.Forms.ListBox();
            this.lblGrad = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbGradovi
            // 
            this.cbGradovi.FormattingEnabled = true;
            this.cbGradovi.Location = new System.Drawing.Point(35, 32);
            this.cbGradovi.Name = "cbGradovi";
            this.cbGradovi.Size = new System.Drawing.Size(342, 23);
            this.cbGradovi.TabIndex = 0;
            this.cbGradovi.TextChanged += new System.EventHandler(this.cbGradovi_TextChanged);
            // 
            // lblTemperatura
            // 
            this.lblTemperatura.AutoSize = true;
            this.lblTemperatura.Location = new System.Drawing.Point(176, 150);
            this.lblTemperatura.Name = "lblTemperatura";
            this.lblTemperatura.Size = new System.Drawing.Size(20, 15);
            this.lblTemperatura.TabIndex = 1;
            this.lblTemperatura.Text = "°C";
            // 
            // lblVlaga
            // 
            this.lblVlaga.AutoSize = true;
            this.lblVlaga.Location = new System.Drawing.Point(179, 205);
            this.lblVlaga.Name = "lblVlaga";
            this.lblVlaga.Size = new System.Drawing.Size(17, 15);
            this.lblVlaga.TabIndex = 2;
            this.lblVlaga.Text = "%";
            // 
            // lblTlak
            // 
            this.lblTlak.AutoSize = true;
            this.lblTlak.Location = new System.Drawing.Point(179, 257);
            this.lblTlak.Name = "lblTlak";
            this.lblTlak.Size = new System.Drawing.Size(27, 15);
            this.lblTlak.TabIndex = 3;
            this.lblTlak.Text = "hPa";
            // 
            // lblNajtopliji
            // 
            this.lblNajtopliji.AutoSize = true;
            this.lblNajtopliji.Location = new System.Drawing.Point(65, 334);
            this.lblNajtopliji.Name = "lblNajtopliji";
            this.lblNajtopliji.Size = new System.Drawing.Size(98, 15);
            this.lblNajtopliji.TabIndex = 4;
            this.lblNajtopliji.Text = "Najtopliji gradovi";
            // 
            // lblNajhladniji
            // 
            this.lblNajhladniji.AutoSize = true;
            this.lblNajhladniji.Location = new System.Drawing.Point(239, 334);
            this.lblNajhladniji.Name = "lblNajhladniji";
            this.lblNajhladniji.Size = new System.Drawing.Size(107, 15);
            this.lblNajhladniji.TabIndex = 5;
            this.lblNajhladniji.Text = "Najhladniji gradovi";
            // 
            // lbNajtopliji
            // 
            this.lbNajtopliji.FormattingEnabled = true;
            this.lbNajtopliji.ItemHeight = 15;
            this.lbNajtopliji.Location = new System.Drawing.Point(35, 385);
            this.lbNajtopliji.Name = "lbNajtopliji";
            this.lbNajtopliji.Size = new System.Drawing.Size(153, 169);
            this.lbNajtopliji.TabIndex = 6;
            // 
            // lbNajhladniji
            // 
            this.lbNajhladniji.FormattingEnabled = true;
            this.lbNajhladniji.ItemHeight = 15;
            this.lbNajhladniji.Location = new System.Drawing.Point(224, 385);
            this.lbNajhladniji.Name = "lbNajhladniji";
            this.lbNajhladniji.Size = new System.Drawing.Size(153, 169);
            this.lbNajhladniji.TabIndex = 7;
            // 
            // lblGrad
            // 
            this.lblGrad.AutoSize = true;
            this.lblGrad.Location = new System.Drawing.Point(179, 96);
            this.lblGrad.Name = "lblGrad";
            this.lblGrad.Size = new System.Drawing.Size(0, 15);
            this.lblGrad.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 594);
            this.Controls.Add(this.lblGrad);
            this.Controls.Add(this.lbNajhladniji);
            this.Controls.Add(this.lbNajtopliji);
            this.Controls.Add(this.lblNajhladniji);
            this.Controls.Add(this.lblNajtopliji);
            this.Controls.Add(this.lblTlak);
            this.Controls.Add(this.lblVlaga);
            this.Controls.Add(this.lblTemperatura);
            this.Controls.Add(this.cbGradovi);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbGradovi;
        private System.Windows.Forms.Label lblTemperatura;
        private System.Windows.Forms.Label lblVlaga;
        private System.Windows.Forms.Label lblTlak;
        private System.Windows.Forms.Label lblNajtopliji;
        private System.Windows.Forms.Label lblNajhladniji;
        private System.Windows.Forms.ListBox lbNajtopliji;
        private System.Windows.Forms.ListBox lbNajhladniji;
        private System.Windows.Forms.Label lblGrad;
    }
}

